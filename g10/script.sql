-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema g10
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `g10` ;

-- -----------------------------------------------------
-- Schema g10
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `g10` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `g10` ;

-- -----------------------------------------------------
-- Table `g10`.`Usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Usuario` ;

CREATE TABLE IF NOT EXISTS `g10`.`Usuario` (
  `Nombre` VARCHAR(45) NULL,
  `Correo` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NULL,
  PRIMARY KEY (`Correo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `g10`.`Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Producto` ;

CREATE TABLE IF NOT EXISTS `g10`.`Producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NULL,
  `PrecioSugerido` DOUBLE NULL,
  `Imagen` VARCHAR(100) NULL,
  PRIMARY KEY (`idProducto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `g10`.`Categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Categoria` ;

CREATE TABLE IF NOT EXISTS `g10`.`Categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idCategoria`),
  UNIQUE INDEX `Nombre_UNIQUE` (`Nombre` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `g10`.`Producto-Categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Producto-Categoria` ;

CREATE TABLE IF NOT EXISTS `g10`.`Producto-Categoria` (
  `Producto_idProducto` INT NOT NULL,
  `Categoria_idCategoria` INT NOT NULL,
  PRIMARY KEY (`Producto_idProducto`, `Categoria_idCategoria`),
  INDEX `fk_Producto-Categoria_Categoria1_idx` (`Categoria_idCategoria` ASC),
  CONSTRAINT `fk_Producto-Categoria_Producto`
    FOREIGN KEY (`Producto_idProducto`)
    REFERENCES `g10`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Producto-Categoria_Categoria1`
    FOREIGN KEY (`Categoria_idCategoria`)
    REFERENCES `g10`.`Categoria` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `g10`.`Subscripcion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Subscripcion` ;

CREATE TABLE IF NOT EXISTS `g10`.`Subscripcion` (
  `Categoria_idCategoria` INT NOT NULL,
  `Usuario_Correo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Categoria_idCategoria`, `Usuario_Correo`),
  INDEX `fk_Subscripcion_Usuario1_idx` (`Usuario_Correo` ASC),
  CONSTRAINT `fk_Subscripcion_Categoria1`
    FOREIGN KEY (`Categoria_idCategoria`)
    REFERENCES `g10`.`Categoria` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Subscripcion_Usuario1`
    FOREIGN KEY (`Usuario_Correo`)
    REFERENCES `g10`.`Usuario` (`Correo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `g10`.`Carrito`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Carrito` ;

CREATE TABLE IF NOT EXISTS `g10`.`Carrito` (
  `Usuario_Correo` VARCHAR(45) NOT NULL,
  `Actual` INT NOT NULL,
  PRIMARY KEY (`Usuario_Correo`),
  INDEX `fk_Carrito_Usuario1_idx` (`Usuario_Correo` ASC),
  CONSTRAINT `fk_Carrito_Usuario1`
    FOREIGN KEY (`Usuario_Correo`)
    REFERENCES `g10`.`Usuario` (`Correo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `g10`.`Detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `g10`.`Detalle` ;

CREATE TABLE IF NOT EXISTS `g10`.`Detalle` (
  `Producto_idProducto` INT NOT NULL,
  `Carrito_Usuario_Correo` VARCHAR(45) NOT NULL,
  `PrecioFinal` DOUBLE NOT NULL,
  `Cantidad` INT NOT NULL,
  PRIMARY KEY (`Producto_idProducto`, `Carrito_Usuario_Correo`),
  INDEX `fk_Detalle_Carrito1_idx` (`Carrito_Usuario_Correo` ASC),
  CONSTRAINT `fk_Detalle_Producto1`
    FOREIGN KEY (`Producto_idProducto`)
    REFERENCES `g10`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Detalle_Carrito1`
    FOREIGN KEY (`Carrito_Usuario_Correo`)
    REFERENCES `g10`.`Carrito` (`Usuario_Correo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
