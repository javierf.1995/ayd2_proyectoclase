import React from 'react';

import './App.css';
import Header from './Components/Header';
import MainContent from './Components/MainContent';
import Content from './Components/Content';
import Catalogo from './Components/Catalogo';
import Usuario from './Components/Usuario';


import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";

function App() {
    return ( 
    <div className = "App" >

<Router>
      <div>
      <Header/>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/Registrar">
            <Usuario/>
          </Route>


          <Route path="/IniciarSesion">
                    
          </Route>
          <Route path="/Tabla">
            <Catalogo/>
          </Route>

          <Route path="/Mision">

          </Route>


          <Route path="/">
            <MainContent/>
          </Route>
        </Switch>
      </div>
    </Router>
        
        

        </div>
    );
}

export default App;