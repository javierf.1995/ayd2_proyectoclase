import React from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';
import logo from '../logo.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart, faSignInAlt, faUserPlus } from '@fortawesome/free-solid-svg-icons';


class Header extends React.Component {
  render() {
    return (
      <div>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand href="/">
            <img src={logo} className="d-inline-block align-top" alt="logo" width="30" height="30" />
            G10</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/">Home</Nav.Link>
              <NavDropdown title="Categorias" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Casa Inteligente</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Computadoras</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Audio</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
      <Nav.Link href="#Mision">Mision</Nav.Link>
            </Nav>
            <Nav>
              <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-info">Search</Button>
              </Form>
              <Nav.Link href="/Tabla">
                <FontAwesomeIcon icon={faShoppingCart} color="white" />
                {' Carrito'}
              </Nav.Link>
              <Nav.Link href="/IniciarSesion">
                <FontAwesomeIcon icon={faSignInAlt} color="white" />
                {' Iniciar Sesion'}
              </Nav.Link>
              <Nav.Link href="/Registrar">
                <FontAwesomeIcon icon={faUserPlus} color="white" />
                {' Registrarse'}
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}


export default Header;